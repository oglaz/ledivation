import React, { createContext, useContext, useEffect, useState } from 'react';
import axios from 'axios';
import svgImage from '../public/LEDIVATION.svg';
import svgImageHeader from '../public/LEDIVATION_LAMPAN.svg';
import Banner from '../public/LEDIVATION_Banner.svg'

const AppContext = createContext();

export function AppWrapper({ children }) {
    const [fetchedImages, setFetchedImages] = useState(null);

    const fetchImage = async () => {
        const requestOne = axios.get(svgImage.src);
        const requestTwo = axios.get(svgImageHeader.src);
        const requestThree = axios.get(Banner.src);

        await axios.all([requestOne, requestTwo, requestThree])
            .then((resp) => { return { Landing: resp[0].data, Logo: resp[1].data, Banner: resp[2].data } })
            .then((response) => {
                setFetchedImages(response);
            });
    };

    useEffect(() => {
        fetchImage();
    }, []);

    if (!fetchedImages) {
        return null;
    }

    return (
        <AppContext.Provider value={fetchedImages}>
            {children}
        </AppContext.Provider>
    );
}

export function useAppContext() {
    return useContext(AppContext);
}
import React, { useEffect } from "react";
import Image from "next/image";
import anime from "animejs";
import email from "../public/email.svg";
import { Contact, Wrapper } from "../styles/about";

import useWindowSize from "../hook/windowSize";

export default function About() {
  const window = useWindowSize();
  useEffect(() => {
    anime({
      targets: "#contact",
      easing: "easeOutQuint",
      autoplay: true,
      translateY: [50, 0],
      opacity: [0, 1],
    });
  }, []);
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <Wrapper
        style={{ paddingTop: window.width < 760 ? "20px" : "100px" }}
        padding={window.width < 760 ? 0 : 200}
      >
        <Contact>
          <div
            style={{
              fontSize: window.width < 500 ? "16px" : "25px",
              fontWeight: 300,
              color: "#fff",
              marginBottom: window.width < 500 ? "20px" : "0px",
            }}
            id="contact"
          >
            Feel free to contact us for a project, inquire about a
            collaboration, or simply say hello.
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              fontSize: window.width < 500 ? "18px" : "25px",
              alignItems: "center",
              color: "#fff",
            }}
          >
            <Image
              alt="Landing Image"
              layout="intrinsic"
              loading="eager"
              src={email}
              height="100"
              width="50"
            />
            <div style={{ marginLeft: "10px" }}>info@ledivation.se</div>
          </div>
        </Contact>
      </Wrapper>
    </div>
  );
}

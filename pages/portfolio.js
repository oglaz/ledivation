import React, { useEffect } from "react";
import anime from "animejs";
import { Wrapper, Section } from "../styles/about";
import useWindowSize from "../hook/windowSize";
import TileGrid from "../components/Grid";

export default function About() {
  const window = useWindowSize();
  const handleRenderCallback = () => {
    var textWrapper = document.querySelector(".ml10 .letters");
    textWrapper.innerHTML = textWrapper.textContent.replace(
      /\S/g,
      "<span class='letter'>$&</span>"
    );
    setTimeout(() => {
      document.getElementById("letters").style.opacity = 1;
      anime({
        targets: ".ml10 .letter",
        rotateY: [-90, 0],
        opacity: [0, 1],
        duration: 1300,
        delay: (el, i) => 25 * i,
      });
    }, 1000);

    setTimeout(() => {
      anime({
        targets: "#middleImage",
        easing: "easeOutQuint",
        autoplay: true,
        translateY: [50, 0],
        opacity: [0, 1],
        duration: 2000,
      });
    }, 1700);
  };

  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <Wrapper padding={window.width < 760 ? 10 : 20}>
        <Section>
          <div style={{ textAlign: "center" }} id="letters" className="ml10">
            <span className="text-wrapper">
              <span
                style={{
                  padding: "20px",
                  fontSize: window.width < 500 ? "20px" : "50px",
                }}
                className="letters"
              >
                View Our Work
              </span>
            </span>
          </div>
          <div>
            <TileGrid onRenderCallback={() => handleRenderCallback()} />
          </div>
        </Section>
      </Wrapper>
    </div>
  );
}

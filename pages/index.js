import React, { useEffect } from "react";
import { useAppContext } from "../context/state";
import useWindowSize from "../hoook/useWindow";
import Image from "next/image";

import landing from "../public/landing.jpg";

import Head from "next/head";
import { Wrapper } from "../styles/common";

import anime from "animejs";
import Video from "../components/video";

export default function Home() {
  const { width } = useWindowSize();
  const fetchedImages = useAppContext();

  useEffect(() => {
    if (fetchedImages) {
      const landingLogoPaths = document
        .getElementById("landingLogo")
        .querySelectorAll("path");

      // convert `NodeList` to an array
      const landingPaths = Array.from(landingLogoPaths);

      landingPaths.forEach((path) => {
        path.style.transformOrigin = "50% 50%";
        path.style.transformBox = "fill-box";
        path.style.opacity = 0;
      });

      //loggo animation in
      setTimeout(() => {
        anime({
          targets: landingPaths,
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: anime.stagger(20),
          easing: "easeInOutCirc",
          autoplay: true,
          scale: [0, 1],
          opacity: [0, 1],
        });
      }, 500);

      setTimeout(() => {
        anime({
          targets: landingPaths,
          strokeDashoffset: [anime.setDashoffset, 0],
          delay: anime.stagger(30),
          easing: "easeInOutCirc",
          autoplay: true,
          scale: [0, 1],
          opacity: [0, 1],
          direction: "reverse",
          delay: 1700,
        });

        anime({
          easing: "easeOutCirc",
          targets: "#landingText",
          translateY: [-30, 0],
          delay: 800,
          opacity: 1,
        });

        anime({
          easing: "easeOutCirc",
          targets: "#landingBeginning",
          translateY: [20, 0],
          delay: 1800,
          opacity: [0, 1],
        });

        anime({
          easing: "easeOutCirc",
          targets: "#landingImage",
          translateY: width > 500 ? [200, 0] : "60px",
          delay: 800,
          opacity: 1,
        });

        anime({
          targets: "#header",
          easing: "easeOutQuart",
          autoplay: true,
          translateY: [-100, 0],
          opacity: [0, 1],
          complete: function (anim) {
            anime({
              targets: "#headerLogo",
              easing: "easeOutQuint",
              autoplay: true,
              translateX: [-100, 0],
              opacity: [0, 1],
            });
          },
        });
        anime({
          targets: ".menuItems",
          easing: "easeOutQuart",
          autoplay: true,
          translateY: [100, 0],
          opacity: [0, 1],
          delay: anime.stagger(100),
        });
      }, 2700);
    }
  }, [fetchedImages, width]);

  if (!fetchedImages) {
    return null;
  }

  return (
    <Wrapper
      style={{
        display: width < 500 && "flex",
        flexDirection: width < 500 && "column",
        height: width < 500 && "100vh",
        justifyContent: width < 500 && "center",
      }}
    >
      <Head>
        <title>LEDIVATION</title>
      </Head>
      <div
        style={{
          position: "relative",
          display: "flex",
          justifyContent: "center",
          flexWrap: "wrap",
        }}
      >
        <div
          style={{
            width: "50%",
            minWidth: "500px",
            display: "flex",
            justifyContent: "center",
            color: "white",
            fontSize: width < 500 ? "20px" : "30px",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <div
            id="landingText"
            style={{
              opacity: 0,
              textAlign: "center",
              position: "relative",
              top: "-10%",
              padding: width < 500 && " 70px",
              paddingBottom: width < 500 && "20px",
            }}
          >
            A PASSION FOR CREATING INTERACTIVE ART
          </div>
          <div
            id="landingBeginning"
            style={{
              opacity: 0,
              textAlign: "center",
              position: "relative",
              top: "-5%",
              fontSize: "25px",
              color: "#E90465",
            }}
          >
            This is just the beginning
          </div>
        </div>
        <div
          id="landingImage"
          style={{
            position: "relative",
            width: width < 500 ? "100%" : "50%",
            height: "100%",
            maxHeight: "800px",
            opacity: 0,
          }}
        >
          <Image
            alt="landingImg"
            src={landing}
            width={300} // Ange en giltig bredd här
            height={50} // Ange en giltig höjd här
            layout="responsive"
            objectFit="cover"
          />
        </div>

        <div
          id="landingLogo"
          style={{
            width: width > 500 ? "500px" : "60%",
            zIndex: 2,
            position: "absolute",
            top: "10%",
          }}
          dangerouslySetInnerHTML={{ __html: fetchedImages.Landing }}
        ></div>
      </div>
    </Wrapper>
  );
}

import { AppWrapper } from "../context/state"; // import based on where you put it
import { Layout } from "../components/Layout";
import { Noto_Sans } from "next/font/google";
import "../styles/globals.css";

const noto = Noto_Sans({
  weight: ["100","400"],
  style: ["normal", "italic"],
  subsets: ["latin"],
});

function MyApp({ Component, pageProps }) {
  return (
    <div className={noto.className}>
      <AppWrapper>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </AppWrapper>
    </div>
  );
}

export default MyApp;

import React, { useEffect } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import anime from "animejs";
import { Wrapper } from "../styles/about";
import smallLandingImage from "../public/smallLandingImage.jpg";
import useWindowSize from "../hook/windowSize";

export default function About() {
  const window = useWindowSize();
  const router = useRouter();

  debugger;

  useEffect(() => {
    // var textWrapper = document.querySelector(".ml10 .letters");
    // textWrapper.innerHTML = textWrapper.textContent.replace(
    //   /\S/g,
    //   "<span class='letter'>$&</span>"
    // );
    // setTimeout(() => {
    //   document.getElementById("letters").style.opacity = 1;
    //   anime({
    //     targets: ".ml10 .letter",
    //     rotateY: [-90, 0],
    //     opacity: [0, 1],
    //     duration: 1300,
    //     delay: (el, i) => 25 * i,
    //   });
    // }, 500);

    setTimeout(() => {
      anime({
        targets: "#middleImage",
        easing: "easeOutQuint",
        autoplay: true,
        translateY: [50, 0],
        opacity: [0, 1],
        duration: 2000,
      });
    }, 400);
  }, []);
  console.log("w", window.width);
  return (
    <div style={{ display: "flex", justifyContent: "center" }}>
      <Wrapper padding={window.width < 760 ? 10 : 50}>
        <div
          style={{
            marginTop: window.width < 760 ? "100px" : "150px",
            marginBottom: window.width < 760 ? "20px" : "50px",
          }}
        >
          <div style={{ display: "flex",  }}>
            <span
              style={{
                width: window.width < 760 ? "87%" : "65%",
                lineHeight: window.width < 760 && 1,
              }}
            >
              <span
                style={{
                  fontSize: window.width < 760 ? "20px" : "72px",
                  lineHeight: window.width < 760 ? "22px" : 1.5,
                  width: "70%",
                  color: "white",
                }}
              >
                We are an art collective with a passion for street art.
              </span>
            </span>
          </div>
        </div>
        <div
          id="middleImage"
          style={{ display: "flex", justifyContent: "center", opacity: 0 }}
        >
          <div style={{ width: "100%", display: 'flex', justifyContent: 'center' }} className="unset-img">
            <Image
              alt="Landing Image"
              src={smallLandingImage}
              width={window.width * 0.94}
            />
          </div>
        </div>
        <div
          style={{
            color: "white",
            fontSize: window.width < 760 ? "20px" : "28px",
            width: "100%",
            marginBottom: "120px",
            marginTop: "70px",
            fontWeight: 400,
            maxWidth: "700px",
          }}
        >
          A group of friends got together in Stockholm in early 2016 and formed
          Ledivation. We all have different backgrounds but unite in our passion
          for creating interactive art and going the extra mile to create
          impressive installations on a small budget.
        </div>
        <div
          style={{
            width: "100%",
            height: "480px",
            background: "#F5F5F4",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            fontSize: window.width < 760 ? "20px" : "28px",
            flexDirection: "column",
            textAlign: window.width < 760 ? "center" : "unset",
          }}
        >
          <div>
            Over the years we have allowed us to produce work we are proud of.
          </div>
          <div
            onClick={() => router.push("/portfolio")}
            style={{
              background: "#000000",
              height: "60px",
              width: "200px",
              borderRadius: "50px",
              color: "#fff",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginTop: "20px",
              fontSize: "18px",
              cursor: "pointer",
            }}
          >
            View Our Work
          </div>
        </div>
        <div
          style={{
            color: "#fff",
            fontSize: window.width < 760 ? "20px" : "28px",
            margin: window.width < 760 ? "30px" : "200px",
            marginTop: "200px",
          }}
        >
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              fontSize: "clamp(25px, 4vw, 55px)",
            }}
          >
            OUR JOURNEY
          </div>
          <div style={{ marginTop: "20px", fontWeight: 400 }}>
            {`Much of the Stockholm nightlife consists of established clubs and festivals with big production budgets but often boring results. But a much more vibrant community coexists and numerous events happens in the forests and industrial areas around Stockholm, driven by people who are not doing it for the money, but by a passion to create a different and often unique experience. In this environment our journey began and it's in the simple nature of street art we have our roots.`}
            <br /> <br />
            {`We have compensated for our often tiny budgets with creative innovation, dedication and hard work. We have again and again proved our ability to deliver and impress. We have received art grants from Stockholm Urban Burn and The Borderland Festival numerous times as well as the Nobel Night Cap 2019. We have worked with several different event organizers in Stockholm and have had collaborations with KTH.`}
            <br /> <br />
            {`Apart from this we are dedicated to make everyday life more enjoyable in our local community by adding a touch of simple art. A decorated bus stop for christmas, a box full of secrets in the woods or hanging swings from everything imaginable have brought many smiles to the local residents.`}
            <br /> <br />
            {`However, this is just the beginning. We want to share our art with more people, in more settings and in new locations! But most of all, we want to keep creating and feed our creative spirit!`}
          </div>
        </div>
      </Wrapper>
    </div>
  );
}

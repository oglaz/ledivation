import React, { useState, useEffect } from 'react';
import useWindowSize from '../hoook/useWindow';

export default function Video() {
    const size = useWindowSize();
    const [hasInit, setHasInit] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setHasInit(true)
        }, 2000);
    }, []);

    if (!hasInit) {
        return <div style={{ position: 'fixed', width: '100%', background: '#222', height: '100%', zIndex: -1 }}></div>
    }

    return (
        <video style={{
            position: "fixed",
            width: "100%",
            height: size.width < 780 && "100%",
            objectFit: size.width < 780 && "cover"
        }} autoPlay loop muted >
            <source src={"https://ext.di.se/test/land.mov"} type="video/mp4" />
        </video >
    )

}

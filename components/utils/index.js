export const extractAddressFromString = (urlString) => {
  const urlRegex = /url\("([^"]+)"\)/;
  const match = urlString.match(urlRegex);
  if (match && match.length === 2) {
    return match[1];
  } else {
    return null; // Return null om det inte finns någon matchning
  }
};

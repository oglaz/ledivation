import React, { useEffect, useState, useRef } from "react";
import { useTransition, animated, useSpringRef } from "@react-spring/web";
import Image from "next/image";
import { useRouter } from "next/router";
import { useAppContext } from "../context/state";
import UK from "../public/UK.svg";
import SWE from "../public/SWE.svg";
import arrow from "../public/arrow.svg";
import useWindowSize from "../hoook/useWindow";
import Lottie from "lottie-react";
import hamburgerAnimation from "./hamburger.json";

/* <div id="headerBanner" style={{ width: "180px" }} dangerouslySetInnerHTML={{ __html: fetchedImages.Banner }}></div> */
const menu = ["about", "portfolio", "contact"];
let MenuIsOpen = false;
export default function Header({ animate }) {
  const size = useWindowSize();
  const fetchedImages = useAppContext();
  const router = useRouter();
  const lottieRef = useRef();
  const transRef = useSpringRef();
  const [MenuIsActive, setMenuIsActive] = useState(false);

  useEffect(() => {
    setMenuIsActive(false);
    MenuIsOpen = true;
  }, []);

  useEffect(() => {
    transRef.start();
  }, [MenuIsActive, transRef]);

  const transitions = useTransition(menu, {
    ref: transRef,
    keys: null,
    trail: 500 / menu.length,
    initial: { transform: "translate3d(150%,0,0)" },
    from: {
      opacity: !MenuIsOpen ? 0 : 1,
      transform: !MenuIsOpen ? "translate3d(150%,0,0)" : "translate3d(0%,0,0)",
    },
    enter: {
      opacity: !MenuIsOpen ? 1 : 0,
      transform: !MenuIsOpen ? "translate3d(0%,0,0)" : "translate3d(150%,0,0)",
      padding: "10px",
      textTransform: "uppercase",
    },
    leave: { opacity: 1, transform: "translate3d(-50%,0,0)" },
    reset: true,
  });

  const handleMenuClick = () => {
    MenuIsOpen = !MenuIsOpen;
    setMenuIsActive(!MenuIsActive);
    lottieRef.current.setSpeed(1.7);
    !MenuIsOpen
      ? lottieRef.current.playSegments([0, 30], true)
      : lottieRef.current.playSegments([30, 80], true);
  };

  return (
    <>
      <div
        plain
        id="header"
        style={{
          fontWeight: 300,
          width: "100%",
          height: size.width > 500 ? "120px" : "60px",
          background: "#000000",
          opacity: animate && 0,
          display: "flex",
          justifyContent: size.width > 500 ? "start" : "space-between",
          alignItems: "center",
          fontStyle: "normal",
          letterSpacing: "0rem",
          textTransform: "none",
          lineHeight: "1.8em",
          textDecoration: "none",
          backfaceVisibility: "hidden",
          zIndex: 2,
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <>
          <div
            onClick={() => router.push("/")}
            id="headerLogo"
            style={{
              width: size.width > 500 ? "35px" : "25px",
              opacity: animate && 0,
              cursor: "pointer",
              margin: "5px",
              marginLeft: "20px",
            }}
            dangerouslySetInnerHTML={{ __html: fetchedImages.Logo }}
          ></div>
          {size.width > 500 ? (
            <div style={{ display: "flex" }}>
              <div
                onClick={() => {
                  router.push("/about");
                }}
                className="menuItems"
                style={{
                  color: router.route === "/about" ? "yellow" : "#fff",
                  fontSize: "15px",
                  margin: "20px",
                  cursor: "pointer",
                  zIndex: 2,
                }}
              >
                About
              </div>
              <div
                onClick={() => router.push("/portfolio")}
                className="menuItems"
                style={{
                  color: router.route === "/portfolio" ? "yellow" : "#fff",
                  fontSize: "15px",
                  margin: "20px",
                  cursor: "pointer",
                  zIndex: 2,
                }}
              >
                Portfolio
              </div>
              <div
                onClick={() => {
                  router.push("/contact");
                }}
                className="menuItems"
                style={{
                  color: router.route === "/contact" ? "yellow" : "#fff",
                  fontSize: "15px",
                  margin: "20px",
                  cursor: "pointer",
                  zIndex: 2,
                }}
              >
                Contact
              </div>
            </div>
          ) : (
            <div
              onClick={() => handleMenuClick()}
              style={{
                color: "#fff",
                right: "5%",
                display: "flex",
                position: "relative",
              }}
            >
              <Lottie
                lottieRef={lottieRef}
                animationData={hamburgerAnimation}
                loop={false}
              />
              {size.width < 500 && (
                <div
                  style={{
                    color: "#fff",
                    position: "absolute",
                    right: "-20%",
                    top: "35px",
                    background: MenuIsActive && "#222",
                    zIndex:100
                  }}
                >
                  {transitions((style, item) => (
                    <animated.div
                      onClick={() => {
                        MenuIsOpen = false;
                        setMenuIsActive(false);
                        router.push(`/${item}`);
                      }}
                      style={style}
                    >
                      {item}
                    </animated.div>
                  ))}
                </div>
              )}
            </div>
          )}
        </>
      </div>
    </>
  );
}

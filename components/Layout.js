// components/Layout.js
import React from 'react';
import Header from './header';
import { useRouter } from 'next/router'

export const Layout = ({ children }) => {
    const router = useRouter()
    return (
        <div className='lay'>
            <Header animate={router.route === "/"} />
            {children}
        </div>
    );
}
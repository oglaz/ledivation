import React, { useEffect, useState } from "react";
import useWindowSize from "../../hoook/useWindow";
import tilesData from "./data";
import { Grid, Slug, Fade } from "mauerwerk";
import Image from "next/image";
import { extractAddressFromString } from "../utils";
import { useSpring, animated, config } from "@react-spring/web";

const Tile = ({ tile }) => {
  const [active, setActive] = useState(false);
  const { width } = useWindowSize();

  const scale = useSpring({
    from: {
      transform: !active ? "scale(0.99)" : "scale(0.7)",
    },
    to: {
      transform: !active ? "scale(0.7)" : "scale(0.99)",
    },
  });

  const text = useSpring({
    from: {
      transform: active ? "scale(1)" : "scale(0.2)",
      opacity: active ? 1 : 0,
      color: active ? "#222" : "#fff",
    },
    to: {
      color: active ? "#fff" : "#222",
      opacity: active ? 0 : 1,
      transform: active ? "scale(0.2)" : "scale(1)",
    },
  });

  const textAnimation = useSpring({
    config: config.default,
    from: {
      transform: active
        ? "translateY(0px) scale(0.7)"
        : "translateY(20px) scale(0.99)",
      opacity: active ? 0 : 1,
      color: active ? "#fff" : "#222",
    },
    to: {
      color: active ? "#222" : "#fff",
      fontSize: active ? "28px" : "20px",
      opacity: active ? 1 : 0,
      transform: active
        ? "translateY(20px) scale(0.99)"
        : "translateY(0px) scale(0.7)",
    },
    delay: 250,
  });

  const description = useSpring({
    from: {
      opacity: active ? 1 : 0,
      color: active ? "#222" : "#fff",
    },
    to: {
      opacity: active ? 0 : 1,
      color: active ? "#fff" : "#222",
    },
  });

  const descriptionAnimation = useSpring({
    config: config.default,
    from: {
      transform: active
        ? "translateY(100px) scale(0.7)"
        : "translateY(0px) scale(0.99)",
      opacity: active ? 0 : 1,
      color: active ? "#fff" : "#222",
      fontSize: active ? "15px" : "15px",
    },
    to: {
      color: active ? "#222" : "#fff",
      fontSize: active ? "15px" : "15px",
      opacity: active ? 1 : 0,
      transform: active
        ? "translateY(0px) scale(0.99)"
        : "translateY(100px) scale(0.7)",
    },
    delay: 350,
  });

  return (
    <div
      onMouseOver={() => setActive(true)}
      onMouseLeave={() => setActive(false)}
      style={{
        color: "#222",
        width: width < 500 ? "100%" : "49%",
        background: "#FFF4F4",
        marginBottom: "20px",
        maxHeight: "400px",
        position: "relative",
        cursor: "pointer",
        overflow: "hidden",
      }}
    >
      <div style={{ position: "relative" }}>
        <animated.div
          style={{
            ...scale,
            width: "100%",
            height: "100%",
            maxHeight: "400px",
            overflow: "hidden",
          }}
        >
          <Image
            src={extractAddressFromString(tile.css)}
            alt="Bildbeskrivning"
            width={300} // Ange en giltig bredd här
            height={50} // Ange en giltig höjd här
            layout="responsive"
            objectFit="cover"
          />
        </animated.div>

        <animated.div
          style={{
            ...textAnimation,
            position: "absolute",
            bottom: 50,
            display: "flex",
            justifyContent: "center",
            width: "100%",
            fontWeight: 700,
            textTransform: "uppercase",
            zIndex: 2,
            background: "rgba(255,255,255,0.9)",
            fontSize: active
              ? width < 500
                ? "14px"
                : "20px"
              : width
              ? "19px"
              : "28px",
          }}
        >
          <div>{tile.name}</div>
        </animated.div>
        <animated.div
          style={{
            ...descriptionAnimation,
            position: "absolute",
            bottom: 10,
            display: "flex",
            justifyContent: "center",
            width: "100%",
            fontWeight: 700,
            textTransform: "uppercase",
            zIndex: 2,
            background: "rgba(255,255,255,0.9)",
          }}
        >
          <div>{tile.description}</div>
        </animated.div>
      </div>
      <animated.div
        style={{
          ...text,
          position: "absolute",
          top: width < 500 ? 5 : 10,
          display: "flex",
          justifyContent: "center",
          width: "100%",
          fontWeight: 700,
          textTransform: "uppercase",
          zIndex: 2,
          fontSize: active
            ? width < 500
              ? "14px"
              : "20px"
            : width < 500
            ? "14px"
            : "28px",
        }}
      >
        {tile.name}
      </animated.div>
      <animated.div
        style={{
          ...description,
          position: "absolute",
          bottom: width < 500 ? 10 : 20,
          display: "flex",
          justifyContent: "center",
          width: "100%",
          fontWeight: 700,
          textTransform: "uppercase",
          zIndex: 2,
          fontSize: active
            ? width < 500
              ? "12px"
              : "18px"
            : width < 500
            ? "12px"
            : "18px",
        }}
      >
        {tile.description}
      </animated.div>
    </div>
  );
};

const TileGrid = ({ onRenderCallback }) => {
  const data = tilesData;

  useEffect(() => {
    // Kör callback-funktionen när komponenten har renderats första gången
    onRenderCallback();
  }, [onRenderCallback]);

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        flexWrap: "wrap",
        justifyContent: "space-between",
      }}
    >
      {data.map((tile) => (
        <Tile key={tile.name} tile={tile} />
      ))}
    </div>
  );
};

export default TileGrid;

// Gradients taken from: https://webgradients.com/
const data= [
    {
      name: 'Warp Drive',
      description: 'The Space Nomads Forest',
      css: 'url("https://ext.di.se/test/nobelnight.jpg")',
      video: "https://ext.di.se/test/warp.mp4",
      text: `The Space Nomads Forest was one of our first projects. The first version appeared at Stockholm Urban Burn in 2016 and was funded by an art grant. This version had one large mushroom that would react to how close people were standing to it, and it was accompained by 10 smaller mushrooms. We recieved a second art grant to bring an even larger version to the Borderland festival the same year. This consisted of three large mushrooms, the biggest at 7m tall with a cuddle puddle in the stem and a panel with dials and buttons to control the programs of the mushrooms. There was also 15 smaller mushrooms of various sizes. Various smaller iterations of the forest have made an apperance at a number of events around Stockhom afterwards, notably at the main stage of KTHs Quarnevalen, Stockholm Urban Burn 2017 and Goodville Festival 2017.`
    },
    {
      name: 'Platonic Love',
      description: 'The five platonics',
      css: 'url("https://ext.di.se/test/platonic.jpg")',
      video: "https://ext.di.se/test/platonic.mp4",
      text: `The five platonic solids on a massive scale, with a gyro in the tetrahedron which change the colors of the other solids when pushed. It's made with wooden frames and 90 programmable LED tubes. These are designed and produced by us using PVC piping and 3D printed parts to keep the cost down. It was originally made for The Borderland Festival in Denmark 2017 funded by an art grant from the festival. We received a second art grant to bring an updated version to Stockholm Urban Burn 2018. Parts of the installation have since been displayed at a number of events in Stockholm.`
    },
    {
      name: 'Space Nomads Forest',
      description: 'Led mushrooms',
      css: 'url("https://ext.di.se/test/svamp.jpg")',
      video: "https://ext.di.se/test/svamp.mp4",
      text: `The five platonic solids on a massive scale, with a gyro in the tetrahedron which change the colors of the other solids when pushed. It's made with wooden frames and 90 programmable LED tubes. These are designed and produced by us using PVC piping and 3D printed parts to keep the cost down. It was originally made for The Borderland Festival in Denmark 2017 funded by an art grant from the festival. We received a second art grant to bring an updated version to Stockholm Urban Burn 2018. Parts of the installation have since been displayed at a number of events in Stockholm.`
    },
    {
      name: 'TeleportWheel',
      description: 'LaserWire',
      css: 'url("https://ext.di.se/test/teleport.jpg")',
      video: "https://ext.di.se/test/teleport.mp4",
      text: `The TeleportWheel was originally funded by a very small grant and made for Afterglow 2017, however the idea was to use this as a prototype for a much larger version using LaserWire, which we have not found funding for yet. It consists of a total of 40m of EL-wire which are arranged so that four green and four yellow strand can be controlled independantly.`
    },
    {
      name: 'Le Drum',
      description: 'The led drum',
      css: 'url("https://ext.di.se/test/drum.jpg")',
      video: "https://ext.di.se/test/drum.mp4",
      text: `LeDrum was part of an installation called Galaxy Pontonen at Liljeholmsbron for Nobel Week Light 2021. It has since then been around several dancefloors around Stockholm.

      The idea is to encourage people to help cocreate the party by having a drum connected to the lights of the dance floor. If noone is banging the drum, the place goes almost dark and a collective effort is made to keep it alive. It is made with a drum with LED and sensors that can be passed around the dancefloor, and it's connected to a total of 30m of LED strips spread as a star above. The drum will make different colors and intensity depending on how hard and how you hit it.`
    }
  ]
  

  export default data;
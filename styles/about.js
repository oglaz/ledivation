import styled from "styled-components";
// Create a Wrapper component that'll render a <section> tag with some styles
export const Wrapper = styled.div`
  padding: ${(props) => `${props.padding}px`};
  padding-top: 30px;
  padding-bottom: 0px;
  max-width: 2200px;
   width: 100%;
`;

export const Section = styled.section`
  margin-top: 100px;
  margin-bottom: 30px;
`;

export const Contact = styled.section`
  margin-top: 200px;
  width: 100%;
  text-align: center;
`;

import styled from 'styled-components';

// Create a Wrapper component that'll render a <section> tag with some styles
export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;
    justify-content: center;
`;

export const Section = styled.section`
  margin-top: 100px;
  width: 790px;
  margin-bottom: 30px;
`;
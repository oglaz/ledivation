import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 100px;
  display: flex;
`;


export const Contact = styled.section`
  width: 100%;
  text-align: center;
  height: 90vh;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;